﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IWSK_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private RS232Port rsPort = new RS232Port();
        private Logger logger = new Logger();
        private DataFormat dataFormat = DataFormat.ASCII;
        private Parser parser = new Parser();
        Timer pingTimer = new Timer();
        Stopwatch pingWatch = new Stopwatch();
        List<byte> pingList = new List<byte>();
        List<byte> pongList = new List<byte>();
        byte[] pongArr = new byte[] { 80, 79, 78, 71 };
        byte[] pingArr = new byte[] { 80, 73, 78, 71 };
        bool connectedDuePing = false;
        private bool isTimeout = false;

        public MainWindow()
        {
            InitializeComponent();
            this.ReloadPorts();
            this.initCombos();

            rsPort.Connected += rsPort_Connected;
            rsPort.Disconnected += rsPort_Disconnected;
            rsPort.DataReceived += rsPort_DataReceived;
            rsPort.DataSent += rsPort_DataSent;
            rsPort.TransactionFinished += (sender, e) => logger.LogMessage("Transakcja zakończona " + (e.TransactionResult ? "powodzeniem" : "niepowodzeniem"));
            pingTimer.Elapsed += pingTimer_Tick;
            rsPort.PinChanged += (sender, e) => UpdatePins();
            parser.FrameParsed += parser_FrameParsed;
        }

        private void initCombos()
        {
            this.baudRateComboBox.ItemsSource = new object[]
            {
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "56000",
            "57600",
            "115200",
            "128000",
            "256000"
            };
            this.baudRateComboBox.SelectedIndex = 4;
            this.stopBitCombo.ItemsSource = new object[] {
            "1",
            "2"};
            this.stopBitCombo.SelectedIndex = 0;
            this.flowControlCombo.ItemsSource = new object[] {
            "Brak kontroli",
            "Kontrola sprzętowa",
            "Kontrola programowa"};
            this.flowControlCombo.SelectedIndex = 0;
            this.parityCombo.ItemsSource = new object[] {
            "None",
            "Even",
            "Odd"};
            this.parityCombo.SelectedIndex = 0;
            this.bitsCountCombo.ItemsSource = new object[] {
            "7",
            "8"};
            this.bitsCountCombo.SelectedIndex = 1;

        }

        private void ReloadPorts()
        {
            portComboBox.ItemsSource = SerialPort.GetPortNames();

            if (portComboBox.Items.Count>0)
            {
                portComboBox.SelectedIndex = 0;
                connectButton.IsEnabled = true;
            }
            else
            {
                logger.LogMessage("Nie odnaleziono portów COM w komputerze.");
                connectButton.IsEnabled = false;
            }
        }


        private void rsPort_Connected(object sender , EventArgs e)
        {
            connectButton.Content = "Rozłącz";
            connectButton.Click -= connectButton_Click;
            connectButton.Click += disconnectButton_Click;
            UpdatePins();
            rtsCheckBox.IsChecked = rsPort.RTSEnable;
            dtrCheckBox.IsChecked = rsPort.DTREnable;

            sendButton.IsEnabled = true;

             
        }

        private void rsPort_Disconnected(object sender, EventArgs e)
        {
            InvokeOrNot(() =>
            {
                connectButton.Content = "Połącz";
                connectButton.Click -= disconnectButton_Click;
                connectButton.Click += connectButton_Click;
            }, connectButton);
            InvokeOrNot(() => sendButton.IsEnabled = false, sendButton);
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            if (!rsPort.Disconnect())
                logger.LogMessage("Nie można zamknąć portu.");
        }



        private void scanButton_Click(object sender, RoutedEventArgs e)
        {
            this.ReloadPorts();
        }

        private void connectButton_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                rsPort.SetConnectionParameters(portComboBox.SelectedItem.ToString(),
                    Convert.ToInt32(baudRateComboBox.Text),
                    RS232Port.ParityArray(parityCombo.SelectedIndex),
                    Convert.ToInt32(bitsCountCombo.SelectedItem),
                    RS232Port.StopBitsArray(stopBitCombo.SelectedIndex));

                if (!rsPort.Connect())
                {
                    logger.LogMessage("Nie można otworzyć portu: " + rsPort.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("Podano niepoprawne argumenty: " + ex.Message);
            }
        }


        private void pingButton_Click(object sender, RoutedEventArgs e)
        {
            isTimeout = false;
            if (!rsPort.IsOpen)
            {
                connectButton_Click(sender, e);
                connectedDuePing = true;
            }

            if (!rsPort.Send(pingArr, true))
            {
                logger.LogMessage("Nie można zrobić PING: nie można ustanowić połączenia.");
            }
            pingWatch.Reset();
            pingWatch.Start();
            int interval;

            if (int.TryParse(pingTimeOutText.Text, out interval))
            {
                pingTimer.Interval = interval;
            }
            else
            {
                pingTimeOutText.Text = (pingTimer.Interval = 100).ToString();
            }
        
            pingTimer.Start();            
            pingButton.IsEnabled = false;
        }

        private void rsPort_DataReceived(object sender, EventArgs e)
        {
            byte[] data = rsPort.GetReadBytes().ToArray();

            TerminatePing(data);

            if (pingWatch.IsRunning)
            {
                pongList.AddRange(data.Take(pongArr.Length - pongList.Count));

                if (pongList.Count == pongArr.Length )
                {

                    if (IsPongData())
                    {
                        if (!isTimeout)
                        {
                            pingTimer.Enabled = false;
                            pingTimer.Stop();
                            logger.LogMessage("PING: " + pingWatch.Elapsed.Milliseconds + "ms");

                            data = data.Skip(pongList.Count).Take(data.Length - pongList.Count).ToArray();
                        }
                    }
                    else
                    {
                        logger.LogMessage("PING: " + pingWatch.Elapsed + "\nNiepoprawne dane.");
                    }

                    PingStop();
                }
            }

            if (!connectedDuePing)
                //readDataTextBox.AppendText(StringParser.ByteToDisplay(data));
                InvokeOrNot(() =>
                {
                    readDataTextBox.AppendText(StringParser.ByteToDisplay(data));
                    parser.AppendToParser(data);
                }, readDataTextBox);
        }

        private void PingStop(bool timeout = false)
        {
            pingTimer.Stop();

            if (timeout)
            {
                logger.LogMessage("PING: Timeout");
                isTimeout = true;
            }

            pingWatch.Stop();
            pongList.Clear();

            InvokeOrNot(() => pingButton.IsEnabled = true, pingButton);

            if (connectedDuePing)
            {
                disconnectButton_Click(null, null);
            }
        }

        private void rsPort_DataSent(object sender, SentEventArgs e)
        {
            if (!pingWatch.IsRunning)
            {
                InvokeOrNot(() => writeDataBox.AppendText(StringParser.ByteToDisplay(e.Buff, dataFormat)), writeDataBox);
            }
        }

        private bool IsPongData()
        {
            for (int i = 0; i < pongArr.Length; i++)
            {
                if (pongList[i] != pongArr[i])
                {
                    return false;
                }
            }

            return true;
        }


        private void TerminatePing(byte[] data)
        {

            foreach (var b in data)
            {
                pingList.Add(b);


                if (b == pingArr[pingList.Count - 1])
                {
                    if (pingList.Count == pingArr.Length)
                    {
                        rsPort.Send(pongArr, true);
                        pingList.Clear();
                    }
                }
                else
                {
                    pingList.Clear();
                }
            }

        }

        private void InvokeOrNot(Action f, Control ctrl)
        {
            //if (System.Windows.Threading.Dispatcher.CurrentDispatcher.CheckAccess())
            //    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(f);
            //else
            //    f.Invoke();

            if (Application.Current.Dispatcher.CheckAccess())
            {
                ctrl.Dispatcher.Invoke(f);
            }
            else
            {
                //Other wise re-invoke the method with UI thread access
                Application.Current.Dispatcher.Invoke(new System.Action(() => InvokeOrNot(f,ctrl)));
            }
        }

        private void UpdatePins()
        {
            InvokeOrNot(() => dsrCheckBox.IsChecked = rsPort.DSR, dsrCheckBox);
            InvokeOrNot(() => ctsCheckBox.IsChecked = rsPort.CTS, ctsCheckBox);
        }

        private void pingTimer_Tick(object sender, EventArgs e)
        {
            PingStop(true);
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            if (rsPort.IsOpen)
            {
                bool bug = false;

             
                    List<byte> sendByte = new List<byte>(StringParser.StrToByteArray(sendTextBox.Text));

                    if (parser.Terminator != null)
                    {
                        sendByte.AddRange(parser.Terminator);
                    }

                    if (!rsPort.Send(sendByte.ToArray()))
                    {
                        bug = true;
                        logger.LogMessage("Nie można wysłać danych: " + rsPort.ErrorMessage);
                    }
                    else
                    {
                        //if (delTextCheckBox.Checked)
                          //  sendTextBox.Clear();
                    }
                


                if (!bug && transactionBox.IsChecked.Value)
                {
                    InitTransaction();
                }
            }
        }

        private void InitTransaction()
        {
            if (rsPort.IsTransaction)
            {
                logger.LogMessage("Aktualnie wykonywana już jest transakcja, nie można wysłać danych jako transakcji.");
                return;
            }

            int interval;

            if (!int.TryParse(transactionTextBox.Text, out interval))
            {
                transactionTextBox.Text = (pingTimer.Interval = 100).ToString();
            }

            rsPort.SetTransaction(interval);
        }

        private void parser_FrameParsed(object sender, EventArgs e)
        {
            InvokeOrNot(() => readFrameListBox.Items.Add(StringParser.ByteToDisplay(parser.Frame, dataFormat)), readFrameListBox);
           // InvokeOrNot(() => scrollToEnd(),readFrameListBox);
        }

        private void crButton_Click(object sender, RoutedEventArgs e)
        {
            terminatorText.Text += "\\r";
        }

        private void lfButton_Click(object sender, RoutedEventArgs e)
        {
            terminatorText.Text += "\\n";
        }

        private void scrollToEnd()
        {
            readFrameListBox.SelectedIndex = readFrameListBox.Items.Count - 1;
            readFrameListBox.UpdateLayout();
            readFrameListBox.ScrollIntoView(readFrameListBox.SelectedItem);
         

        }

    

        private void setTerminatorButton_Click(object sender, RoutedEventArgs e)
        {
        
            parser.Terminator = StringParser.StrToByteArray(terminatorText.Text);
            logger.LogMessage("Ustawiono terminator: " + terminatorText.Text);
        }

 

        private void dtrCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rsPort.DTREnable = dtrCheckBox.IsChecked.Value;
            }
            catch (NullReferenceException)
            {
                if (dtrCheckBox.IsChecked.Value)
                    dtrCheckBox.IsChecked = false;
            }
        }

        private void rtsCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rsPort.RTSEnable = rtsCheckBox.IsChecked.Value;
            }
            catch (NullReferenceException)
            {
                if (rtsCheckBox.IsChecked.Value)
                    rtsCheckBox.IsChecked = false;
            }
        }

        private void asciiButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;

            if (rb!=null && rb.IsChecked.Value)
            {
                dataFormat = (DataFormat)Convert.ToInt32(rb.Tag);
            }

        }

    }
}
